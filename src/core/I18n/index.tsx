import {I18nManager} from 'react-native';
import * as RNLocalize from 'react-native-localize';
import i18n from 'i18n-js';
import DefaultLanguage from 'translations/en.json';
import memoize from 'lodash.memoize';
export type TranslationKeys = keyof typeof DefaultLanguage;
const getLanguageObject = (language: TranslationKeys) => {
  const result = Object.fromEntries(
    Object.entries(language).map(([key, val]) => [
      key,
      val || DefaultLanguage[key as TranslationKeys],
    ]),
  );
  return result;
};
const translationGetters = {
  en: () => DefaultLanguage,
  vi: () => getLanguageObject(require('translations/vi.json')),
};

export const translate = memoize(
  (key: TranslationKeys, config?: Object) => i18n.t(key, config),
  (key, config) => (config ? key + JSON.stringify(config) : key),
);

export const setI18nConfig = async (languageCode?: string) => {
  const fallback = {languageTag: 'en', isRTL: false};
  let {languageTag, isRTL} =
    RNLocalize.findBestAvailableLanguage(Object.keys(translationGetters)) ||
    fallback;
  languageTag = languageCode || languageTag;
  translate.cache.clear;
  I18nManager.forceRTL(isRTL);
  i18n.translations = {
    [languageTag]:
      translationGetters[languageTag as keyof typeof translationGetters](),
  };
  i18n.fallbacks = false;
  i18n.locale = languageTag;
};
